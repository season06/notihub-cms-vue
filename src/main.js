import Vue from 'vue'
import Vuetify from 'vuetify'
import vuetify from '@/plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import TopBar from './components/TopBar.vue'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


new Vue({
  vuetify,
  render: h => h(TopBar),
}).$mount('#topbar')
